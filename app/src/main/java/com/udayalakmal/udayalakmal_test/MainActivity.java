package com.udayalakmal.udayalakmal_test;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button button_signin;
    TextView tv_emailValidation;
    TextView tv_passwordValidation;
    EditText et_email;
    EditText et_password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initalize();
        clickListners();
    }

    private void initalize(){

        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_password);
        tv_passwordValidation = findViewById(R.id.password_validation);
        tv_emailValidation = findViewById(R.id.email_validation);
        button_signin =  findViewById(R.id.bt_signin);


    }

    private void clickListners(){
        button_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                tv_emailValidation.setText("");
                tv_passwordValidation.setText("");
                tv_emailValidation.setVisibility(View.GONE);
                tv_passwordValidation.setVisibility(View.GONE);


                String email = et_email.getText().toString();
                String password = et_password.getText().toString();
                if(email.equals("") || !checkVlaidation(email)) {
                    tv_emailValidation.setVisibility(View.VISIBLE);
                    tv_emailValidation.setText(R.string.email_validation);
                }

                if(password.equals("")) {
                    tv_passwordValidation.setVisibility(View.VISIBLE);
                    tv_passwordValidation.setText(R.string.password_validation);
                }

            }
        });
    }


    private boolean checkVlaidation(String email){
      return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();

    }


}
